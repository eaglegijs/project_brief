# Name: Customer_Area_Map_Functions.R
# Author: Gijs van Lith
# Date: 19-06-2018

# Comments:
# Please make sure Java is installed

# ===============================================
# ===============================================
# Load packages


packages <- c( "EVR", "rgdal", "ggplot2", "ggmap", "xlsx", "tools", "argparser")

package.check <- lapply(packages, FUN = function(x) {
  if (!require(x, character.only = TRUE)) {
    install.packages(x, dependencies = TRUE)
    library(x, character.only = TRUE)
  }
})


# ===============================================
# ===============================================
# Functions


# Copy or unzip KML files 
copy_or_unpack <- function(filestr, result_fold){
  
  # If it is a KML file, just copy it
  if(grepl('kml$', filestr)){
    copyFile(srcPathname = filestr,destPathname = result_fold, overwrite = T)
  }
  
  # If it is a KMZ file, unpack it and rename KML files to name of KMZ file 
  else if(grepl('kmz$', filestr)){
    
    # Get KMZ filename
    nameprep <- basename(path = file_path_sans_ext(x = filestr))
    # Temp folder name
    outdr <- paste(result_fold, nameprep, sep = "/")
    
    # Unzip to temp folder
    files <- unzip(zipfile = filestr, overwrite = T, exdir = outdr)
    
    # Rename KMLs and place with correct name in results folder
    # If there is only 1 file, just copy, else copy and rename with index
    if(length(files)>1){
      for(i in seq_along(files)){
        if(grepl('kml$', files[i])){
          file.rename(from = files[i] , to = paste0(result_fold,"/", nameprep, i, ".kml" ))
        }
      }
    }
    else{
      if(grepl('kml$', files[i])){
        file.rename(from = files , to = paste0(result_fold,"/", nameprep, ".kml" ))
      }
    }
    
    unlink(x = outdr, recursive = T, force = T)
    
  }
  else{
    "Not a KML or KMZ file"
  }
  return("")
}

# Function that opens KML area file, and returns spatialpolygondataframe
load_kml_ar <- function(path_to_kml){
  
  # import shape and transform to lat long
  import <- ogrListLayers(path_to_kml)
  aoi_sh <- readOGR(dsn = path_to_kml, layer = import[1][1]) 
  aoi_sh <- spTransform(aoi_sh, CRS("+init=epsg:4326"))
  
  for(i in seq_along(import)){
    if(class(aoi_sh) == "SpatialPointsDataFrame" ||class(aoi_sh) == "SpatialPoints"){
      aoi_sh <- readOGR(dsn = path_to_kml, layer = import[i])
    }
  }
  return(aoi_sh)
}

# in_file is string
create_images <- function(in_file){
  
  title_prep <- basename(path = file_path_sans_ext(x = in_file))
  
  if(is.na(title_prep)){title_prep = "AOI"}
  
  print(paste0("start with ", file_path_sans_ext(title_prep)))
  
  # Execute load KML function
  aoi <- load_kml_ar(in_file)
  
  # Get extent
  aoi <- spTransform(aoi, CRS("+init=epsg:4326"))
  ext <- extent(aoi)
  long <- mean(c(ext@xmax, ext@xmin))
  lat <- mean(c(ext@ymax,ext@ymin))
  
  # find tile width of object
  long_diff <- ext@xmax-ext@xmin
  lat_diff <- ext@ymax-ext@ymin
  max_diff <- max(long_diff, lat_diff)
  print(paste0("width of area is ", str(max_diff), "degrees of longitudes"))
  
  # select zoom level of map according to object width. Tile width is width in degrees
  zoom_level <- seq(0,19)
  tile_width <- c(360,180,90,45,22.5,11.25,
                  5.625,2.813,1.406,0.703,0.352,
                  0.176,0.088,0.044,0.022,0.011,
                  0.005,0.003,0.001,0.0005)
  zoom_check <- data.frame(zoom_level, tile_width)
  res <- zoom_check$zoom_level[which.min(abs(max_diff-zoom_check$tile_width))]
  
  # Get mapdata
  basemap <- get_map(location= c(long, lat), zoom=res, source = "google", maptype = "hybrid")
  
  # Create map
  mp <-ggmap(basemap,  extent = "device", margins = 2) + geom_polygon(data = aoi,
                                                                      aes(x=long, y = lat, group = group ), 
                                                                      colour = "red",fill = "black", alpha = 0.1, size = 1)+ 
    ggtitle(title_prep)+ 
    theme(plot.margin = margin(0.2, 0.2, 0.2, 0.2, "cm"),
          plot.title=element_text(size=14, hjust = 0.5))
  ggsave(mp, file = paste0(title_prep, ".png"), width = 14, height = 14, units = "cm")
  
  return("")
}


# Function to calculate area oer aoi and return for dataframe
aoi_hec_calc <- function(in_file){

  # extract name from path
  title_prep <- basename(path = file_path_sans_ext(x = in_file))

  if(is.na(title_prep)){title_prep = "AOI"}
  
  # Execute load KML function
  aoi <- load_kml_ar(in_file)
  
  # Calculate hectares
  hects <- round(as.numeric(area(x = aoi))/10000, digits = 3)
  
  outfile <- c(title_prep, hects)
  return(outfile)
}

